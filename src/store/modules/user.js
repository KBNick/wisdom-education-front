export default {
  namespaced: true,
  state: {
    token: null,
    userInfo: null,
    webSocketConnectState: 0
  },
  mutations: {
    updateToken (state, token) {
      state.token = token
      localStorage.setItem('token', token)
    },

    updateUserInfo (state, userInfo) {
      state.userInfo = userInfo
      localStorage.setItem('userInfo', JSON.stringify(userInfo))
    },

    updateWebSocketConnectState (state, webSocketConnectState) {
      state.webSocketConnectState = webSocketConnectState
      sessionStorage.setItem('webSocketConnectState', JSON.stringify(webSocketConnectState))
    }
  },

  getters: {
    getToken (state) {
      if (state.token) {
        return state.token
      }
      return localStorage.getItem('token')
    },

    getUserInfo (state) {
      if (state.userInfo) {
        return state.userInfo
      }
      return JSON.parse(localStorage.getItem('userInfo'))
    },

    getWebSocketConnectState (state) {
      if (state.webSocketConnectState) {
        return state.course
      }
      let value = sessionStorage.getItem('webSocketConnectState')
      if (!value) {
        return null
      }
      return JSON.parse(value)
    }
  }
}
