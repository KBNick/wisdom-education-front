export default {
  namespaced: true,
  state: {
    webSiteConfig: null,
    videoUrl: null,
    testPaperInfo: null,
    host: 'http://120.79.144.34',
    fileHost:  'https://education-prod-1253719016.cos.ap-nanjing.myqcloud.com',
    webSocketHost: '120.79.144.34',
    // host: 'http://localhost',
    //fileHost: 'http://localhost/uploads',
    //webSocketHost: 'localhost',
    proxyHttp: false // 是否开启http代理
  },

  mutations: {

    updateWebSiteConfig (state, webSiteConfig) {
      state.webSiteConfig = webSiteConfig
      sessionStorage.setItem('webSiteConfig', JSON.stringify(webSiteConfig))
    },

    updateTestPaperInfo (state, testPaperInfo) {
      state.testPaperInfo = testPaperInfo
      sessionStorage.setItem('testPaperInfo', JSON.stringify(testPaperInfo))
    },

    updateVideoUrl (state, data) {
      state.videoUrl = data
      sessionStorage.setItem('videoUrl', JSON.stringify(data))
    }

  },

  getters: {

    getWebSiteConfig (state) {
      if (state.webSiteConfig != null) {
        return state.webSiteConfig
      }
      return JSON.parse(sessionStorage.getItem('webSiteConfig'))
    },

    getTestPaperInfo (state) {
      if (state.testPaperInfo != null) {
        return state.testPaperInfo
      }
      return JSON.parse(sessionStorage.getItem('testPaperInfo'))
    },

    getVideoUrl (state) {
      if (state.videoUrl) {
        return state.videoUrl
      }
      return JSON.parse(sessionStorage.getItem('videoUrl'))
    }
  }
}
